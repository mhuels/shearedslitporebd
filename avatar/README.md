  * ovito configuration.in
  * modifications:
    * Color coding
      * Input property: Position.Z
      * End value: 0.5
      * Start value: -0.5
  * Viewport: Front (click on it)
  * zoom in (14 scrolls)
  * Render Active Viewport
    * Single frame
    * Background: Transparent
    * Output image size: 640x480
    * Render output: Save to file: png

