//
// Created by mhuels on 7/9/20.
//

#ifndef SHEAREDSLITPOREBD_CARTESIAN_C_H
#define SHEAREDSLITPOREBD_CARTESIAN_C_H

#include "cartesian_coordinate.h"
#include <complex>

using REAL_C = CARTESIAN_COORDINATE<double>;
using COMPLEX_C = CARTESIAN_COORDINATE<complex<double>>;

#endif //SHEAREDSLITPOREBD_CARTESIAN_C_H