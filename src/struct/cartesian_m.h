//
// Created by mhuelsberg on 30.07.20.
//

#ifndef CARTESIAN_MATRIX_H
#define CARTESIAN_MATRIX_H

#include "cartesian_matrix.h"
#include <complex>

using REAL_M = CARTESIAN_MATRIX<double>;
using COMPLEX_M = CARTESIAN_MATRIX<complex<double>>;

#endif // CARTESIAN_MATRIX_H
