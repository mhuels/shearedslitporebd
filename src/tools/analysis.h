//
// Created by mhuels on 3/31/20.
//

#ifndef SHEAREDSLITPOREBD_ANALYSIS_H
#define SHEAREDSLITPOREBD_ANALYSIS_H

#include "../struct/cartesian_c.h"
#include <vector>

REAL_C getMinCoordinate(vector<REAL_C> coordinateList);

#endif //SHEAREDSLITPOREBD_ANALYSIS_H
