//
// Created by mhuels on 4/20/20.
//

#ifndef SHEAREDSLITPOREBD_FORMAT_H
#define SHEAREDSLITPOREBD_FORMAT_H

#include <string>

using namespace std;

string surroundWithSeparator(string str, int length = 60, int numberOfLines = 1, char sign = '#', bool spaces = true);

#endif //SHEAREDSLITPOREBD_FORMAT_H
