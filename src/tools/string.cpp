//
// Created by mhuelsberg on 05.08.21.
//

#include "string.h"

bool str_is_empty(string str){
    return str.empty();
}
