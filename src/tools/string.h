//
// Created by mhuelsberg on 05.08.21.
//

#ifndef SHEAREDSLITPOREBD_STRING_H
#define SHEAREDSLITPOREBD_STRING_H

#include <string>
using namespace std;

bool str_is_empty(string str);

#endif //SHEAREDSLITPOREBD_STRING_H
