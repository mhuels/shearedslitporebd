#include "argparse.h"

#include <iostream>

using namespace std;

ARGUMENT_PARSER::ARGUMENT_PARSER(int argc, const char* argv[]){
    try{
        addOptions();
        //decompose command line arguments and put them into variablesMap
        po::store(parse_command_line(argc, argv, options), variablesMap);
        po::notify(variablesMap);

        if(variablesMap.count("help")){
            cout << options << endl;
            exit(0);
        }
    }
    catch(const exception& e){
        cerr << e.what() << endl;
    }
}

//could be divided into logical segments
void ARGUMENT_PARSER::addOptions(){
    options.add_options()
            ("help,h", "Help screen")
            ("settings,s", po::value<string>()->default_value(SETTINGS_IN), "settings file")
            ("version,v", po::bool_switch()->default_value(false), "print version number and exit")
            ("dry", po::bool_switch()->default_value(false), "do a dry run")
            ("clear", po::bool_switch()->default_value(CLEAR),
             "clear all existing output files (before simulation start)")
            ("numberOfTimesteps,N", po::value<double>(), "Number of timesteps the simulations runs for")
            ("duration,d", po::value<double>(), "Duration (in Brownian times) the simulations runs for.\n"
                                                "Overwrites --numberOfTimesteps/-N")
            ("numberOfPeriods", po::value<double>(), "Number of oscillation periods the simulations runs for.\n"
                                                     "Overwrites --numberOfTimesteps/-N and --duration/-d")
            ("skip", po::value<double>()->default_value(SKIP), "skip the first x timesteps")
            ("skipDuration", po::value<double>(),
             "Same as --skip but in units of Brownian time.\n"
             "Overwrites --skip")
            ("skipPeriod", po::value<double>(), "Same as --skip but in units of oscillation periods.\n"
                                                       "Overwrites --skip and --skipDuration")
            ("restart", po::bool_switch()->default_value(RESTART),
             "try to restart a previously aborted simulation")
            ("milestone", po::value<double>()->default_value(MILESTONE), "save restart snapshots every x timesteps")
            ("milestoneDuration", po::value<double>(),
             "Same as --milestone but in units of Brownian time.\n"
             "Overwrites --milestone")
            ("milestonePeriod", po::value<double>(), "Same as --milestone but in units of oscillation periods.\n"
                                                       "Overwrites --milestone and --milestoneDuration")
            ("milestoneRuntime", po::value<double>()->default_value(MILESTONE_RUNTIME),
                    "save restart snapshots every x (runtime) seconds")
            ("milestoneRuntimeOffset", po::value<double>()->default_value(MILESTONE_RUNTIME_OFFSET),
                    "shifts the --milestoneRuntime forward by x (runtime) seconds;\n"
                    "defaults to 0.2 * milestoneRuntime")
            ("watchdog", po::value<double>()->default_value(WATCHDOG),
                    "interrupt simulation after x (runtime) seconds;\n"
                    "3600=1h, 14400=4h, 86400=1d, 172800=2d")
            ("watchdogOffset", po::value<double>()->default_value(WATCHDOG_OFFSET),
                    "interrupt simulation x seconds before --watchdog;"
                    "grants some buffer to finish the calculation")
             ;
    main.add_options()
            ("shearRate", po::value<double>()->default_value(SHEAR_RATE),
             "(constant) shear rate offset (in units of 1/Brownian time)")
            ("amplitude,a", po::value<double>()->default_value(AMPLITUDE),
             "shear rate amplitude (in units of 1/Brownian time)")
            ("period,p", po::value<double>()->default_value(OSCILLATION_PERIOD),
             "shear rate oscillation period (in units of Brownian time)")
            ("phaseOffset,o", po::value<double>()->default_value(PHASE_OFFSET), "phase offset (in units of Pi)"
                                                                                "(0->cos, -0.5->sin, 1->-cos, 0.5->-sin)")
            ("configuration,c", po::value<string>()->default_value(CONFIGURATION_IN),
             "configuration file (particle positions, simulation box)")
             ;
    secondary.add_options()
            ("mu", po::value<double>()->default_value(MU), "mobility")
            ("kT", po::value<double>()->default_value(KT), "thermal energy")
            ("kappa", po::value<double>()->default_value(KAPPA),
             "inverse Debye screening length of the Yukawa potential (in units of 1/particle diameter)")
            ("yInteractionStrength", po::value<double>()->default_value(Y_INTERACTION_STRENGTH),
             "strength of screened-Coulomb Yukawa potential (in units kT)")
            ("ssInteractionStrength", po::value<double>()->default_value(SS_INTERACTION_STRENGTH),
             "strength of softsphere interaction (in units of kT)")
            ("wallInteractionStrength", po::value<double>()->default_value(WALL_INTERACTION_STRENGTH),
             "strength of wall interaction")
             ;
    numerical.add_options()
            ("dt", po::value<double>()->default_value(0), "length of timestep (in units of Brownian time)")
            ("seed", po::value<unsigned int>()->default_value(0),
             "random number generator seed; 0 = random seed will be generated")
            ("rngCounter", po::value<unsigned long long>()->default_value(0),
             "initial random number generator counter; 0 = no initial increments")
             ;
    observables.add_options()

            ("printStress", po::value<double>()->default_value(PRINT_STRESS), "print stresses every x-th timestep; "
                                                                              "x<0 -> no print-outs")
            ("printStressDuration", po::value<double>(),
             "Same as --printStress but in units of total simulation time.\n"
             "Overwrites --printStress")
            ("printStressPeriod", po::value<double>(), "Same as --printStress but in units of oscillation periods.\n"
                                                       "Overwrites --printStress and --printStressDuration")
            ("printStressFourier", po::value<double>()->default_value(PRINT_STRESS_FOURIER),
             "calculate 0-th to 4-th stress Fourier component using stresses from every x-th timestep; "
             "x<0 -> no Fourier component calculation")
            ("printStressFourierDuration", po::value<double>(),
             "Same as --printStressFourier but in units of total simulation time.\n"
             "Overwrites --printStressFourier")
            ("printStressFourierPeriod", po::value<double>(),
             "Same as --printStressFourier but in units of oscillation periods.\n"
             "Overwrites --printStressFourier and --printStressFourierDuration")
            ("printEnergy", po::value<double>()->default_value(PRINT_ENERGY), "print energies every x-th timestep; "
                                                                              "x<0 -> no print-outs")
            ("printEnergyDuration", po::value<double>(),
             "Same as --printEnergy but in units of total simulation time.\n"
             "Overwrites --printEnergy")
            ("printEnergyPeriod", po::value<double>(), "Same as --printEnergy but in units of oscillation periods.\n"
                                                       "Overwrites --printEnergy and --printEnergyDuration")
            ("printLayerPosition", po::value<double>()->default_value(PRINT_LAYER_POSITION),
             "print layer positions every x-th timestep; "
             "x<0 -> no print-outs")
            ("printLayerPositionDuration", po::value<double>(),
             "Same as --printLayerPosition but in units of total simulation time.\n"
             "Overwrites --printLayerPosition")
            ("printLayerPositionPeriod", po::value<double>(),
             "Same as --printLayerPosition but in units of oscillation periods.\n"
             "Overwrites --printLayerPosition and --printLayerPositionDuration")
            ("printLayerVelocity", po::value<double>()->default_value(PRINT_LAYER_VELOCITY),
             "print velocities every x-th timestep; "
             "x<0 -> no print-outs")
            ("printLayerVelocityDuration", po::value<double>(),
             "Same as --printLayerVelocity but in units of total simulation time.\n"
             "Overwrites --printLayerVelocity")
            ("printLayerVelocityPeriod", po::value<double>(),
             "Same as --printLayerVelocity but in units of oscillation periods.\n"
             "Overwrites --printLayerVelocity and --printLayerVelocityDuration")
            ("printAngularBond", po::value<double>()->default_value(PRINT_ANGULAR_BOND),
             "print the angular bond order parameter every x-th timestep; "
             "x<0 -> no print-outs")
            ("printAngularBondDuration", po::value<double>(),
             "Same as --printAngularBond but in units of total simulation time.\n"
             "Overwrites --printAngularBond")
            ("printAngularBondPeriod", po::value<double>(),
             "Same as --printAngularBond but in units of oscillation periods.\n"
             "Overwrites --printAngularBond and --printAngularBondDuration")
            ("printSnapshots", po::value<double>()->default_value(PRINT_SNAPSHOTS),
             "Save a configuration snapshot every x-th timestep; "
             "x<0 -> no snapshots")
            ("printSnapshotsDuration", po::value<double>(),
             "Same as --printSnapshots but in units of total simulation time.\n"
             "Overwrites --printSnapshots")
            ("printSnapshotsPeriod", po::value<double>(),
             "Same as --printSnapshots but in units of oscillation periods.\n"
             "Overwrites --printSnapshots and --printSnapshotsDuration")
            ("printPairCorrelation", po::value<double>()->default_value(PRINT_PAIR_CORRELATION),
             "print intra-layer pair correlation function every x-th timestep; "
             "x<0 -> no print-outs")
            ("printPairCorrelationDuration", po::value<double>(),
             "Same as --printPairCorrelation but in units of total simulation time\n"
             "Overwrites --printPairCorrelation")
            ("printPairCorrelationPeriod", po::value<double>(),
             "Same as --printPairCorrelation but in units of oscillation periods.\n"
             "Overwrites --printPairCorrelation and --printPairCorrelationDuration")
            ("printAll", po::value<double>()->default_value(PRINT_ALL), "print all properties every x-th timestep; "
                                                                        "x<0 -> no print-outs; "
                                                                        "ATTENTION: The calculation will be very slow! "
                                                                        "Gets overwritten by other (non-zero) print-arguments")
            ;
    options.add(main).add(secondary).add(numerical).add(observables);
}

ARGUMENTS ARGUMENT_PARSER::parseArgs(){
    ARGUMENTS args;
    args.settingsIn = variablesMap["settings"].as<string>();
    args.configurationIn = variablesMap["configuration"].as<string>();
    args.shearRate = variablesMap["shearRate"].as<double>();
    args.amplitude = variablesMap["amplitude"].as<double>();
    args.oscillationPeriod = variablesMap["period"].as<double>();
    args.phaseOffset = variablesMap["phaseOffset"].as<double>();
    args.kappa = variablesMap["kappa"].as<double>();
    args.yInteractionStrength = variablesMap["yInteractionStrength"].as<double>();
    args.ssInteractionStrength = variablesMap["ssInteractionStrength"].as<double>();
    args.wallInteractionStrength = variablesMap["wallInteractionStrength"].as<double>();
    args.dt = variablesMap["dt"].as<double>();
    args.kT = variablesMap["kT"].as<double>();
    args.mu = variablesMap["mu"].as<double>();
    if(variablesMap.count("numberOfTimesteps")){
        args.numberOfTimesteps = round(variablesMap["numberOfTimesteps"].as<double>());
    }
    else{
        args.numberOfTimesteps = 0;
    }
    if(variablesMap.count("duration")){
        args.setDuration(variablesMap["duration"].as<double>());
    }
    if(variablesMap.count("numberOfPeriods")){
        args.setNumberOfPeriods(variablesMap["numberOfPeriods"].as<double>());
    }
    args.seed = variablesMap["seed"].as<unsigned int>();
    args.rngCounter = variablesMap["rngCounter"].as<unsigned long long>();

    //doubles are allowed as input for convenience (allows 1e5 float terminology)
    args.skip = round(variablesMap["skip"].as<double>());
    if(variablesMap.count("skipDuration")){
        args.skip.setDuration(variablesMap["skipDuration"].as<double>());
    }
    if(variablesMap.count("skipPeriod")){
        args.skip.setPeriod(variablesMap["skipPeriod"].as<double>());
    }
    //
    args.milestone = round(variablesMap["milestone"].as<double>());
    if(variablesMap.count("milestoneDuration")){
        args.milestone.setDuration(variablesMap["milestoneDuration"].as<double>());
    }
    if(variablesMap.count("milestonePeriod")){
        args.milestone.setPeriod(variablesMap["milestonePeriod"].as<double>());
    }
    args.milestoneRuntime = variablesMap["milestoneRuntime"].as<double>();
    args.milestoneRuntimeOffset = variablesMap["milestoneRuntimeOffset"].as<double>();
    args.watchdog = variablesMap["watchdog"].as<double>();
    args.watchdogOffset = variablesMap["watchdogOffset"].as<double>();
    //
    args.printStress = round(variablesMap["printStress"].as<double>());
    if(variablesMap.count("printStressDuration")){
        args.printStress.setDuration(variablesMap["printStressDuration"].as<double>());
    }
    if(variablesMap.count("printStressPeriod")){
        args.printStress.setPeriod(variablesMap["printStressPeriod"].as<double>());
    }
    //
    args.printStressFourier = round(variablesMap["printStressFourier"].as<double>());
    if(variablesMap.count("printStressFourierDuration")){
        args.printStressFourier.setDuration(variablesMap["printStressFourierDuration"].as<double>());
    }
    if(variablesMap.count("printStressFourierPeriod")){
        args.printStressFourier.setPeriod(variablesMap["printStressFourierPeriod"].as<double>());
    }
    //
    args.printEnergy = round(variablesMap["printEnergy"].as<double>());
    if(variablesMap.count("printEnergyDuration")){
        args.printEnergy.setDuration(variablesMap["printEnergyDuration"].as<double>());
    }
    if(variablesMap.count("printEnergyPeriod")){
        args.printEnergy.setPeriod(variablesMap["printEnergyPeriod"].as<double>());
    }
    //
    args.printLayerPosition = round(variablesMap["printLayerPosition"].as<double>());
    if(variablesMap.count("printLayerPositionDuration")){
        args.printLayerPosition.setDuration(variablesMap["printLayerPositionDuration"].as<double>());
    }
    if(variablesMap.count("printLayerPositionPeriod")){
        args.printLayerPosition.setPeriod(variablesMap["printLayerPositionPeriod"].as<double>());
    }
    //
    args.printLayerVelocity = round(variablesMap["printLayerVelocity"].as<double>());
    if(variablesMap.count("printLayerVelocityDuration")){
        args.printLayerVelocity.setDuration(variablesMap["printLayerVelocityDuration"].as<double>());
    }
    if(variablesMap.count("printLayerVelocityPeriod")){
        args.printLayerVelocity.setPeriod(variablesMap["printLayerVelocityPeriod"].as<double>());
    }
    //
    args.printAngularBond = round(variablesMap["printAngularBond"].as<double>());
    if(variablesMap.count("printAngularBondDuration")){
        args.printAngularBond.setDuration(variablesMap["printAngularBondDuration"].as<double>());
    }
    if(variablesMap.count("printAngularBondPeriod")){
        args.printAngularBond.setPeriod(variablesMap["printAngularBondPeriod"].as<double>());
    }
    //
    args.printSnapshots = round(variablesMap["printSnapshots"].as<double>());
    if(variablesMap.count("printSnapshotsDuration")){
        args.printSnapshots.setDuration(variablesMap["printSnapshotsDuration"].as<double>());
    }
    if(variablesMap.count("printSnapshotsPeriod")){
        args.printSnapshots.setPeriod(variablesMap["printSnapshotsPeriod"].as<double>());
    }
    //
    args.printPairCorrelation = round(variablesMap["printPairCorrelation"].as<double>());
    if(variablesMap.count("printPairCorrelationDuration")){
        args.printPairCorrelation.setDuration(variablesMap["printPairCorrelationDuration"].as<double>());
    }
    if(variablesMap.count("printPairCorrelationPeriod")){
        args.printPairCorrelation.setPeriod(variablesMap["printPairCorrelationPeriod"].as<double>());
    }
    //
    args.printAll = round(variablesMap["printAll"].as<double>());
    // overwrite print-statements by printAll if not further specified
    if(args.printAll > 0){
        if(args.printStress == PRINT_STRESS
           && args.printStress.getDuration() == 0
           && args.printStress.getPeriod() == 0)
            args.printStress = args.printAll;
        if(args.printStressFourier == PRINT_STRESS_FOURIER
           && args.printStressFourier.getDuration() == 0
           && args.printStressFourier.getPeriod() == 0)
            args.printStressFourier = 1;
        if(args.printEnergy == PRINT_ENERGY
           && args.printEnergy.getDuration() == 0
           && args.printEnergy.getPeriod() == 0)
            args.printEnergy = args.printAll;
        if(args.printLayerPosition == PRINT_LAYER_POSITION
           && args.printLayerPosition.getDuration() == 0
           && args.printLayerPosition.getPeriod() == 0)
            args.printLayerVelocity = args.printAll;
        if(args.printLayerVelocity == PRINT_LAYER_VELOCITY
           && args.printLayerVelocity.getDuration() == 0
           && args.printLayerVelocity.getPeriod() == 0)
            args.printLayerVelocity = args.printAll;
        if(args.printAngularBond == PRINT_ANGULAR_BOND
           && args.printAngularBond.getDuration() == 0
           && args.printAngularBond.getPeriod() == 0)
            args.printAngularBond = args.printAll;
        if(args.printSnapshots == PRINT_SNAPSHOTS
           && args.printSnapshots.getDuration() == 0
           && args.printSnapshots.getPeriod() == 0)
            args.printSnapshots = args.printAll;
        if(args.printPairCorrelation == PRINT_PAIR_CORRELATION
           && args.printPairCorrelation.getDuration() == 0
           && args.printPairCorrelation.getPeriod() == 0)
            args.printPairCorrelation = args.printAll;
    }
    args.printVersion = variablesMap["version"].as<bool>();
    args.dry = variablesMap["dry"].as<bool>();
    args.restart = variablesMap["restart"].as<bool>();
    args.clear = variablesMap["clear"].as<bool>();
    return args;
}
