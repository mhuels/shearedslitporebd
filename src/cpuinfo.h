//
// Created by mhuelsberg on 05.08.21.
//

#ifndef SHEAREDSLITPOREBD_CPUINFO_H
#define SHEAREDSLITPOREBD_CPUINFO_H

#include <string>
using namespace std;

string getCPUInfo();
string getRAMInfo();
string getHostName();
string getUserName();

#endif //SHEAREDSLITPOREBD_CPUINFO_H
