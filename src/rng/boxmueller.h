/*
 * boxmueller.h
 *
 *  Created on: Jul 10, 2012
 *  Author: Tarlan Vezirov
 */
#ifndef  boxmueller_h
#define  boxmueller_h

double boxmueller(double mu, double sigma);

#endif

