//
// Created by mhuels on 6/15/20.
//

#include "stress_fourier_components.h"

STRESS_FOURIER_COMPONENTS::STRESS_FOURIER_COMPONENTS(){
}

STRESS_FOURIER_COMPONENTS::STRESS_FOURIER_COMPONENTS(const ARGUMENTS& args){
    setup(args);
}

STRESS_FOURIER_COMPONENTS::STRESS_FOURIER_COMPONENTS(string filename){
    readStressFromFile(filename);
}

STRESS_FOURIER_COMPONENTS& STRESS_FOURIER_COMPONENTS::setup(const ARGUMENTS& args){
    if(args.printStressFourier > 0){
        numberOfTimesteps = args.numberOfTimesteps / args.printStressFourier;
    }
    else{
        numberOfTimesteps = args.numberOfTimesteps;
    }
    period = args.oscillationPeriod;
    amplitude = args.amplitude;
    dt = args.dt;
    stress.clear();
    return *this;
}

COMPLEX_M STRESS_FOURIER_COMPONENTS::calculate(int n){
    COMPLEX_M fc; //fc==fourier component
    complex<double> I(0, 1);
    //sometimes rounds falsely
    int timestepsPerPeriod = period / dt;
    if(stress.size() < numberOfTimesteps){
        cout << "Stress trajectory is not complete yet. Caution is adviced." << endl;
    }
    //only consider full cycles (remove uncompleted last cycle if there is one)
    int N = stress.size() / timestepsPerPeriod * timestepsPerPeriod;
    if(N == 0){
        cout << "Existing data is not large enough (" << stress.size() << "<" << timestepsPerPeriod
             << ") to compute a Fourier component!" << endl;
        return fc;
    }
    COMPLEX_M complex_stress;
    for(int t = 0; t < N; t++){
        double phase = -n * 2 * M_PI * (t * dt) / period;
        complex<double> factor = exp(I * phase); //cos, sin to reduce numerical errors
        complex_stress.real(stress[t]);
        fc += complex_stress * factor;
    }
    fc /= N;
    return fc;
}

double STRESS_FOURIER_COMPONENTS::calculateStorageModulus(){
    //this is probably some combination of real and imaginary part of the 1st harmonic fourier component if the shear rate is not pure cos()
    double fc1positive = calculate(1).xz.imag();
    double fc1negative = calculate(-1).xz.imag();
    double storageModulus = (fc1negative - fc1positive) * 2 * M_PI / (period * amplitude);
    return storageModulus;
}

double STRESS_FOURIER_COMPONENTS::calculateLossModulus(){
    //this is probably some combination of real and imaginary part of the 1st harmonic fourier component if the shear rate is not pure cos()
    double fc1positive = calculate(1).xz.real();
    double fc1negative = calculate(-1).xz.real();
    double lossModulus = (fc1negative + fc1positive) * 2 * M_PI / (period * amplitude);
    return lossModulus;
}

double STRESS_FOURIER_COMPONENTS::calculateStorageModulusOld(){
    //this is probably some combination of real and imaginary part of the 1st harmonic fourier component if the shear rate is not pure cos()
    double firstHarmonicFC = calculate(1).xz.imag();
    double storageModulus = -firstHarmonicFC * 2 * M_PI / (period * amplitude);
    return storageModulus;
}

double STRESS_FOURIER_COMPONENTS::calculateLossModulusOld(){
    //this is probably some combination of real and imaginary part of the 1st harmonic fourier component if the shear rate is not pure cos()
    double firstHarmonicFC = calculate(1).xz.real();
    double lossModulus = firstHarmonicFC * 2 * M_PI / (period * amplitude);
    return lossModulus;
}

STRESS_FOURIER_COMPONENTS& STRESS_FOURIER_COMPONENTS::addTimestep(const SHEARED_SLITPORE_SYSTEM& sys){
    REAL_M currentStress = sys.getMeanStress();
    stress.push_back(currentStress);
    return *this;
}

int STRESS_FOURIER_COMPONENTS::getNumberOfTimesteps() const{
    return numberOfTimesteps;
}

vector<REAL_M> STRESS_FOURIER_COMPONENTS::getStressList() const{
    return stress;
}

double STRESS_FOURIER_COMPONENTS::getPeriod() const{
    return period;
}

double STRESS_FOURIER_COMPONENTS::getDt() const{
    return dt;
}

STRESS_FOURIER_COMPONENTS& STRESS_FOURIER_COMPONENTS::setNumberOfTimesteps(int numberOfTimesteps){
    this->numberOfTimesteps = numberOfTimesteps;
    return *this;
}

STRESS_FOURIER_COMPONENTS& STRESS_FOURIER_COMPONENTS::setStressList(vector<REAL_M> stress){
    this->stress = stress;
    return *this;
}

STRESS_FOURIER_COMPONENTS& STRESS_FOURIER_COMPONENTS::setPeriod(double period){
    this->period = period;
    return *this;
}

STRESS_FOURIER_COMPONENTS& STRESS_FOURIER_COMPONENTS::setDt(double dt){
    this->dt = dt;
    return *this;
}

STRESS_FOURIER_COMPONENTS& STRESS_FOURIER_COMPONENTS::readStressFromFile(string filename){
    cout << "readStressFromFile() is not yet implemented!" << endl;
    return *this;
}

ostream& operator<<(ostream& os, const STRESS_FOURIER_COMPONENTS& fc){
    int timestepsPerPeriod = fc.period / fc.dt;
    int numberOfPeriods = fc.stress.size() / timestepsPerPeriod;
    int N = numberOfPeriods * timestepsPerPeriod;
    os << "STRESS_FOURIER_COMPONENTS(numberOfPeriods: " << numberOfPeriods << ")";
    return os;
}
