#ifndef DLVO_INTERACTION_H
#define DLVO_INTERACTION_H

#include "../interfaces/force_template_interfaces.h"
#include "lennard_jones_interaction.h"
#include "../struct/charged_particle.h"
#include "../interfaces/box_geometry.h"
#include "../defaults.h"

class DLVO_SOFTSPHERE_INTERACTION: public TWO_BODY_CONSERVATIVE_FORCE<CHARGED_PARTICLE>{
private:

public:
    DLVO_SOFTSPHERE_INTERACTION();
    DLVO_SOFTSPHERE_INTERACTION(double diameter, double ssInteractionStrength, double yInteractionStrength, double kappa);
    void setup();

    double lengthRange = 10;    //needed for cutOff calculation, default questionable

    double diameter = DIAMETER;
    double ssInteractionStrength = SS_INTERACTION_STRENGTH;
    double yInteractionStrength = Y_INTERACTION_STRENGTH;
    double kappa = KAPPA;

    ///////////////////////////////////// interaction parameters /////////////////////////////////////
    double calculateKappa(int Z = 35, double rho = 0.85, double I = 1e-5, double T = 298, double d = 26e-9, double eps = 78.5);
    double calculateInteractionStrength(double kappaIn, int Z = 35, double T = 298, double d_SI = 26e-9, double eps = 78.5);

    ////////////////////////////////////////// Calculators ///////////////////////////////////////////

    double energyOnParticleFromParticle(CHARGED_PARTICLE& particle1, CHARGED_PARTICLE& particle2, BOX_GEOMETRY& simBox) override;
    REAL_C forceOnParticleFromParticle(CHARGED_PARTICLE& particle1, CHARGED_PARTICLE& particle2, BOX_GEOMETRY& simBox) override;

    [[nodiscard]] double energy(double r) const;
    [[nodiscard]] double forceAbs(double r) const;
    [[nodiscard]] double energyShifted(double r) const;
    [[nodiscard]] double forceAbsShifted(double r) const;

    //semi-private parameters/functions
    double energyCutOffThreshold, forceCutOffThreshold;
    double cutOffRadius, forceShift, energyShift;
    void calculateCutOffThresholds(double rLJ = 3.);

    double calculateCutOffRadius();
    void calculateShifts(double cutOffRadiusIn);

    //file/stream-handling
    friend ostream& operator<<(ostream& os, const DLVO_SOFTSPHERE_INTERACTION& dlvo);
};

#endif // DLVO_INTERACTION_H
